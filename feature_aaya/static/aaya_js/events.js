// IMPLEMENT JQUERY HERE

$(document).ready(function () {
  $("#attend_right").click(function () {
    $("#socmedr").toggle();
  });

  $("#attend").click(function () {
    $("#socmed").toggle();
  });

  $("#attend3").click(function () {
    $("#socmed3").toggle();
  });

  $("#hovertwo").hover(function () {
    $(this).css('cursor', 'pointer').attr('title', 'This will take you to a form for you to register your');
  }, function () {
    $(this).css('cursor', 'auto');
  });

  $("#hoverthis").hover(function () {
    $(this).css('cursor', 'pointer').attr('title', 'See if you can attend any!');
  }, function () {
    $(this).css('cursor', 'auto');
  });

});

//   https://www.sitepoint.com/scroll-based-animations-jquery-css3/
function check_if_in_view() {
  var window_height = $(window).height();
  var window_top_position = $(window).scrollTop();
  var window_bottom_position = (window_top_position + window_height);

  $.each($('.animation-element'), function () {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    if ((element_bottom_position >= window_top_position) &&
      (element_top_position <= window_bottom_position)) {
      $element.addClass('in-view');
    } else {
      $element.removeClass('in-view');
    }
  });
}

$(window).on('scroll resize', check_if_in_view);
$(window).trigger('scroll');

// IMPLEMENT AJAX JQUERY HERE

$(document).on('submit', '#post-form', function (e) {
  e.preventDefault();
  $.ajax({
    type: 'POST',
    url: '/events/preform/',
    data: {
      event_name: $('#event_name').val(),
      event_date: $('#event_date').val(),
      event_desc: $('#event_desc').val(),
      contact: $('#contact').val(),
      csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
      action: 'go'
    },
    success: function (json) {
      document.getElementById("post-form").reset();
      $('.posts').append('<div class="col-md-6">' +
        '<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">' +
        '<div class="col p-4 d-flex flex-column position-static">' +
        '<h3 class="mb-0">' + json.event_name + '</h3>' +
        '</div>' +
        '</div>' +
        '</div>'
      )
    },
    error: function (xhr, errmsg, err) {
      console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
    }
  });
});