
from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from .models import Events

# Create your views here.
# tambahin loginrequired di setiap views yg hrs login dulu
@login_required(login_url="landingPage:signin") 
def events(request):
    return render(request, 'events.html')

# fungsi untuk form
@login_required(login_url="landingPage:signin")
def preform(request):
    events = Events.objects.all()
    response_data = {}
    
    if request.POST.get('action') == 'go':
        event_name = request.POST.get('event_name')
        event_date = request.POST.get('event_date')
        event_desc = request.POST.get('event_desc')
        contact = request.POST.get('contact')

        response_data['event_name'] = event_name
        response_data['event_date'] = event_date
        response_data['event_desc'] = event_desc
        response_data['contact'] = contact

        Events.objects.create(
            event_name = event_name,
            event_date = event_date,
            event_desc = event_desc,
            contact = contact,
        )

        return JsonResponse(response_data)
        
    var = {
        'events':events
    }

    return render(request, 'preform.html', var)
