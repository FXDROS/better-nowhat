# Generated by Django 3.1.3 on 2021-01-02 11:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feature_aaya', '0003_auto_20210102_1822'),
    ]

    operations = [
        migrations.AlterField(
            model_name='events',
            name='event_date',
            field=models.DateField(max_length=100),
        ),
    ]
