from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps

from .models import Events
from .views import events, preform
from .apps import FeatureAayaConfig

import json

# Create your tests here.
class TestingApp(TestCase):
    def test_app(self):
        self.assertEqual(FeatureAayaConfig.name, 'feature_aaya')
        self.assertEqual(apps.get_app_config('feature_aaya').name, 'feature_aaya')

class TestingModel(TestCase):
    def setUp(self):
        self.events = Events.objects.create(
            event_name = "perak",
            event_date = "2000-02-01",
            event_desc = "fun",
            contact = "0812 aaya"
        )

    def test_instance(self):
        self.assertEqual(Events.objects.count(), 1)

class TestingViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.Events = Events.objects.create(
            event_name="perak",
            event_date="2000-02-01",
            event_desc="fun",
            contact="0812 aaya"
        )
        self.events = reverse("events:events")
        self.preform = reverse("events:preform")

        self.userData = {
            'username': 'demo',
            'email': 'apaaja@gmail.com',
            'password1': 'ssrifdiza',
            'password2': 'ssrifdiza',
        }

        self.user = User.objects.create_user(
            'demo', 'apaaja@gmail.com', 'ssrifdiza')
       
        self.login = {
            'username': 'demo',
            'password': 'ssrifdiza'
        }

        self.client.post('/signin/', data=self.login, follow=True)

    def test_events_get(self): 
        response = self.client.get(self.events)
        self.assertEqual(response.status_code, 200)

    def test_preform_get(self):
        response = self.client.get(self.preform)
        self.assertEqual(response.status_code, 200)


    def test_preform_post(self):
        response = self.client.post(
            self.preform,
            {
                "event_name" : "perak",
                "event_date" : "2000-02-01",
                "event_desc" : "fun",
                "contact" : "0812 aaya"
            },
            follow=True
            )
        self.assertEqual(response.status_code, 200)

    def test_json_function(self):
        data = {
            "event_name" : "perak",
            "event_date" : "2000-02-01",
            "event_desc" : "fun",
            "contact" : "0812 aaya"
            }
        response = self.client.post('/events/preform/', json.dumps(data), content_type="application/json")
        self.assertEqual(response.status_code, 200)

class TestingUrls(TestCase):
    def setUp(self):
        self.Events = Events.objects.create(
            event_name="perak",
            event_date="2000-02-01",
            event_desc="fun",
            contact="0812 aaya"
        )
        self.events = reverse("events:events")
        self.preform = reverse("events:preform")

    def test_events_function(self):
        found = resolve(self.events)
        self.assertEqual(found.func, events)
    
    def test_preform_function(self):
        found = resolve(self.preform)
        self.assertEqual(found.func, preform)
    