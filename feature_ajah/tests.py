from django.test import TestCase
from django.test import Client 
from django.urls import resolve, reverse
from .models import MoviesModel
from .views import listForm, forms, movies
from .forms import MoviesForm
from .apps import FeatureAjahConfig
from django.contrib.auth.models import User


# Create your tests here.

class TestModel(TestCase):
    def setUp(self):
        self.MoviesModel = MoviesModel.objects.create(
            movie_name="La la land", rating="7", review="bagus"
        )

    def test_instance_created(self):
        self.assertEqual(MoviesModel.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.MoviesModel), "La la land")


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.MoviesModel = MoviesModel.objects.create(
            movie_name="La la land", rating="7", review="bagus"
        )
        self.forms = reverse("movies:forms")
        self.listForm = reverse("movies:listForm")
        self.movies = reverse("movies:movies")
        self.userData = {
            'username': 'demo',
            'email': 'apaaja@gmail.com',
            'password1': 'ssrifdiza',
            'password2': 'ssrifdiza',
        }
        self.user = User.objects.create_user(
            'demo', 'apaaja@gmail.com', 'ssrifdiza')
        self.login = {
            'username': 'demo',
            'password': 'ssrifdiza'
        }

    def test_GET_movies(self):
        response = self.client.get(self.movies)
        self.assertEqual(response.status_code, 302)

    def test_GET_forms(self):
        response = self.client.get(self.forms)
        self.assertEqual(response.status_code,302)

    def test_POST_forms(self):
        response = self.client.post(self.forms,{'movie_name': 'La la land', 'rating': "7", 'review':"bagus"}, follow = True)
        self.assertEqual(response.status_code, 200)

    def test_POST_forms_invalid(self):
        response = self.client.post(self.forms,{'movie_name': '', 'rating': "7", 'review':""}, follow = True)
        self.assertTemplateUsed(response, 'signin.html')
    
    def test_GET_listForm(self):
        response = self.client.get(self.listForm)
        self.assertEqual(response.status_code, 302)

    def test_fungsi_data(self):
        response = self.client.get('/feature_ajah/data&query=elsa')
        self.assertEqual(response.status_code,404)
    

    def test_url_login(self):
        c = Client()
        response = c.get("/signin")
        self.assertEqual(response.status_code, 301)

    def test_url_default(self):
        c = Client()
        response = c.get("/")
        self.assertEqual(response.status_code, 200)

    def test_url_logout(self):
        c = Client()
        response = c.get("/signup")
        self.assertEqual(response.status_code, 301)
    
    def test_register_users(self):
        toBeRegistered = {
            "username": "demojuga",
            "email": "cinoy@gmail.com",
            "password1": "cinoyganteng",
            "password2": "cinoyganteng",
        }

        response = self.client.post(
            '/signup/', data=toBeRegistered, follow=True)
        self.assertEqual(User.objects.all().count(), 2)



class TestUrls(TestCase):
    def test_forms_use_right_function(self):
        found = resolve('/movies/forms/')
        self.assertEqual(found.func, forms)

    def test_listForm_use_right_function(self):
        found = resolve('/movies/listForm/')
        self.assertEqual(found.func,listForm)

    def test_movies_use_right_function(self):
        found = resolve('/movies/')
        self.assertEqual(found.func,movies)


class TestForm(TestCase):
    def test_form_valid(self):
        form = MoviesForm(
            data={
                "movie_name": "La la land",
                "rating": "7",
                "review": "bagus"
            }
        )
        self.assertTrue(form.is_valid())

    def test_form_invalid(self):
        form = MoviesForm(data={})
        self.assertFalse(form.is_valid())

    
class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(FeatureAjahConfig.name, 'feature_ajah')
