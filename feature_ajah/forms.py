from django import forms

class MoviesForm(forms.Form):
    movie_name = forms.CharField(
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'input-width',
            }

        )

        )
    rating  = forms.IntegerField(
        widget = forms.NumberInput(
            attrs={
                'class':'input-width',
                'min':'0',
                'max':'10'
            }
        )
    )
    review   = forms.CharField(
        max_length = 1000,
        widget = forms.Textarea(
            attrs={
                'class':'input-width',
            }
        )
    )
    