from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from django.http import JsonResponse
import json
import requests

# Create your views here.
from .forms import MoviesForm
from .models import MoviesModel
from django.contrib.auth.decorators import login_required

@login_required(login_url="landingPage:signin") 
def listForm(request):
    movies = MoviesModel.objects.all()
    context = {
        'movies':movies,
    }

    return render(request,'listForm.html',context)

@login_required(login_url="landingPage:signin") 
def forms(request):
    movies_form = MoviesForm()

    if request.method == 'POST':
        MoviesModel.objects.create(
            movie_name       = request.POST.get('movie_name'),
            rating        = request.POST.get('rating'),
            review    = request.POST.get('review'),
        )
        return redirect('movies:listForm')

    context = {
        # 'page_title':'Add course',
        'movies_form':movies_form
    }

    return render(request,'forms.html',context)

@login_required(login_url="landingPage:signin") 
def movies(request):
	return render(request, "movies.html")


@login_required(login_url="landingPage:signin") 
def fungsi_data(request):
    # url = "https://imdb-api.com/en/API/SearchMovie/k_i4t754t5/t=" + request.GET['t']
    url = "https://api.themoviedb.org/3/search/movie?api_key=abc5d6a9364f843cb2eae4da71e1217f&language=en-US&query=" + request.GET['query']


    # url = "http://www.omdbapi.com/?apikey=ab01e7b2" + "t=" + request.GET['t']
    # url = "http://www.omdbapi.com?s="+ request.GET['s']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)


