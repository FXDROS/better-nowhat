$('#nama').ready(function(){
  $("button").click(function(){
    var div = $("#isi"); 
    div.animate({left: '200px'}, "slow");
    div.animate({fontSize: '3em'}, "slow");
  });
});

$('#nama').ready(function(){
  $(".heart.fa").click(function() {
    $(this).toggleClass("fa-heart fa-heart-o");
  });
});  

$(document).ready(function(){
  $('#change').mouseover(function(){
    $('#change').css("background-color", "yellow");
  });
  $('#change').mouseout(function(){
    $('#change').css("background-color", "orange");
  });
});  

$(document).ready(() => {
  $("#keyword").keyup(function(){
      var ketik = $("#keyword").val();
      console.log(ketik);
      let listBuku = $('tbody');
      //Teknik Ajax
      if(ketik.length){
          $.ajax({
            // url : "https://imdb-api.com/en/API/SearchMovie/k_i4t754t5/" + ketik,
            // url: 'data&query=' + ketik,

            url : "https://api.themoviedb.org/3/search/movie?api_key=abc5d6a9364f843cb2eae4da71e1217f&language=en-US&query=" + ketik,

              success: function(data){
                  // let listBuku = $('tbody');
                  var array_items = data.results;
                  $('tbody').empty();
                  // listBuku.empty();
                  console.log(array_items);
                  for(i=0;i < array_items.length;i++){
                      // var genre = array_items[i].description;
                      // var judul = array_items[i].title;
                      // var gambar = array_items[i].image;

                      var genre = array_items[i].release_date;
                      var judul = array_items[i].original_title;
                      var gambar = "https://image.tmdb.org/t/p/w200" + array_items[i].poster_path;

                      if(gambar == false){
                          var img = $('<td>').text("-");
                      }
                      else{
                          var img = $('<td>').append("<img class ='img-fluid' style='width: 10vw' src=" + gambar + ">");
                      }

                      var ttl = $('<td>').append(judul);

                      if(genre == false){
                          var gnr = $('<td>').text("-");
                      }
                      else{
                          var gnr = $('<td>').append(genre);
                      }

                      var content_row = $('<tr>').append(img,ttl,gnr);

                      $('tbody').append(content_row);


                  }
                 

              }
  
          });
      };

      $(listBuku).empty();
  });

});
