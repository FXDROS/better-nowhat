from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps
from .views import index, signin, signup, signout
from .apps import LandingpageConfig


class TestSignin(TestCase):
    def setUp(self):
        self.userData = {
            'username': 'demo',
            'email': 'apaaja@gmail.com',
            'password1': 'ssrifdiza',
            'password2': 'ssrifdiza',
        }
        self.user = User.objects.create_user(
            'demo', 'apaaja@gmail.com', 'ssrifdiza')
        self.login = {
            'username': 'demo',
            'password': 'ssrifdiza'
        }

    def test_create_user(self):
        self.assertEqual(User.objects.all().count(), 1)

    def test_user_login(self):
        response = self.client.post('/signin/', data=self.login, follow=True)
        # print(response.context['user'].is_active)
        self.assertTrue(response.context['user'].is_authenticated)

    def test_register_users(self):
        toBeRegistered = {
            "username": "demojuga",
            "email": "cinoy@gmail.com",
            "password1": "cinoyganteng",
            "password2": "cinoyganteng",
        }

        response = self.client.post(
            '/signup/', data=toBeRegistered, follow=True)
        self.assertEqual(User.objects.all().count(), 2)

    def test_url_login(self):
        c = Client()
        response = c.get("/signin")
        self.assertEqual(response.status_code, 301)

    def test_url_default(self):
        c = Client()
        response = c.get("/")
        self.assertEqual(response.status_code, 200)

    def test_url_logout(self):
        c = Client()
        response = c.get("/signup")
        self.assertEqual(response.status_code, 301)


class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(LandingpageConfig.name, 'landingPage')
        self.assertEqual(apps.get_app_config(
            'landingPage').name, 'landingPage')
