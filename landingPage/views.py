from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.forms import inlineformset_factory
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Create your views here.
from .forms import Register


def index(request):
    return render(request, "index.html")


def signin(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('/')

            else:
                messages.info(request, 'Wrong password/username!')

        response = {}
        return render(request, "signin.html", response)


def signout(request):
    logout(request)
    return redirect('/signin')


def signup(request):
    if request.user.is_authenticated:
        return redirect('/signin')
    else:
        form = Register()
        if request.method == 'POST':
            form = Register(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(
                    request, user + " was created! Please log in to continue.")
                return redirect('/signin')

        response = {
            'form': form
        }

        return render(request, "signup.html", response)
