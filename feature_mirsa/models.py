from django.db import models

# Create your models here.

class Story(models.Model):
    name = models.CharField(max_length=120, verbose_name="Your name")
    experience = models.TextField(blank=True, verbose_name="Your Experience!")

    def __str__(self):
        return self.name