from django.apps import AppConfig


class FeatureMirsaConfig(AppConfig):
    name = 'feature_mirsa'
