from django.contrib.auth.models import User
from django.test import TestCase, Client 
from django.urls import resolve, reverse
from django.apps import apps


from .models import Story
from .views import activities, story, tambah_story
from .forms import StoryForm
from .apps import FeatureMirsaConfig

# Create your tests here.

class TestApp(TestCase):
    def test_apps_is_exist(self):
        self.assertEquals(FeatureMirsaConfig.name, 'feature_mirsa')
        self.assertEquals(apps.get_app_config('feature_mirsa').name, 'feature_mirsa')

class TestModel(TestCase):
    def setUp(self):
        self.Story = Story.objects.create(
            name="aku", experience="bagus"
        )

    def test_instance_created(self):
        self.assertEqual(Story.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.Story), "aku")

class TestForm(TestCase):
    def setUp(self):
        self.tambah_story = reverse("feature_mirsa:tambah_story")
        self.listStory = reverse("feature_mirsa:story")
    
    def test_form_is_valid(self):
        form = StoryForm(
            data={'name':'aku', 'experience':'coba'}
        )
        self.assertTrue(form.is_valid())

    def test_form_invalid(self):
        form = StoryForm(data={})
        self.assertFalse(form.is_valid())

    def test_form_exist(self):
        response = self.client.get(self.tambah_story)
        self.assertEqual(response.status_code, 302)

    def test_story_exist(self):
        response = self.client.get(self.listStory)
        self.assertEqual(response.status_code, 302)

class TestActivitiesViews(TestCase):
    def setUp(self):
        self.activities = reverse("feature_mirsa:activities")
        self.tambah_story = reverse("feature_mirsa:tambah_story")
        self.listStory = reverse("feature_mirsa:story")
        self.client = Client()
        self.userData = {
            'username': 'demo',
            'email': 'apaaja@gmail.com',
            'password1': 'ssrifdiza',
            'password2': 'ssrifdiza',
        }

        self.user = User.objects.create_user(
            'demo', 'apaaja@gmail.com', 'ssrifdiza')
       
        self.login = {
            'username': 'demo',
            'password': 'ssrifdiza'
        }

        self.client.post('/signin/', data=self.login, follow=True)


    def test_activities_url_is_exist(self):
        response = self.client.get(self.activities)
        self.assertEqual(response.status_code, 200)
    
    def test_activities_func(self):
        found = resolve(self.activities)
        self.assertEqual(found.func,activities)

    def test_TambahStory_url_is_exist(self):
        response = self.client.get(self.tambah_story)
        self.assertEqual(response.status_code, 200)
    
    def test_activities_index_func(self):
        found = resolve(self.tambah_story)
        self.assertEqual(found.func, tambah_story)

    def test_ListStory_url_is_exist(self):
        response = self.client.get(self.listStory)
        self.assertEqual(response.status_code, 200)
    
    def test_activities_listStory_func(self):
        found = resolve(self.listStory)
        self.assertEqual(found.func,story)

    def test_ajax(self):
        response = self.client.get(self.activities + "?q=hujan")
        self.assertEqual(response.status_code, 200)


