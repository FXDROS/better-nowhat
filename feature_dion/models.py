from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Music(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    artist = models.CharField(max_length=100)
    link = models.CharField(max_length=1000)
    images = models.CharField(
        max_length=1000, default='http://www.klrc.com/themes/klrcresponsive/images/media-player/album-art-default.png')

    def __str__(self):
        return self.title
