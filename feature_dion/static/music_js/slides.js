var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("card");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  
  // ((Change image every 2 seconds))
  setTimeout(showSlides, 7000); 
}

//Music Update
$(document).ready(function(){
  $('.card').on({
    click: function(){
      if (slideIndex > 2){
        $(this).css('display' , 'none');
        $('.start').css('display' , 'block');
        slideIndex = 0;
      } else {
        $(this).css('display' , 'none');
        $(this).next().css('display' , 'block');
      }
      slideIndex++;
    }
  });
});

//Music Hover
$(document).ready(function(){
  $('.nos').on({
    mouseenter: function(){
      $(this).children('.fix-overlay').css(
        {"display":" flex" , 'justify-content': 'center', 'align-items' : 'center', 'flex-direction' : 'column'});
    },
    mouseleave: function(){
      $(this).children('.fix-overlay').css("display" , "none");
    }
  });
});

//Music Share Form
$(document).ready(function(){
  $("#id_title").focus(function(){
    $(this).css({'background-color' : '#f2954e' , 'color' : '#ffffff'});
  });
  $("#id_title").blur(function(){
    $(this).css({'background-color' : '#0a5687' , 'color' : '#ffffff'});
  });
});

$(document).ready(function(){
  $("#id_artist").focus(function(){
    $(this).css({'background-color' : '#f2954e' , 'color' : '#ffffff'});
  });
  $("#id_artist").blur(function(){
    $(this).css({'background-color' : '#0a5687' , 'color' : '#ffffff'});
  });
});

$(document).ready(function(){
  $("#id_link").focus(function(){
    $(this).css({'background-color' : '#f2954e' , 'color' : '#ffffff'});
  });
  $("#id_link").blur(function(){
    $(this).css({'background-color' : '#0a5687' , 'color' : '#ffffff'});
  });
});

$(document).ready(function(){
  $("#id_images").focus(function(){
    $(this).css({'background-color' : '#f2954e' , 'color' : '#ffffff'});
  });
  $("#id_images").blur(function(){
    $(this).css({'background-color' : '#0a5687' , 'color' : '#ffffff'});
  });
});