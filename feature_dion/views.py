from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
# Create your views here.
from .forms import Music_Form
from .models import Music


@login_required(login_url="landingPage:signin")
def index(request):
    music = Music.objects.all()
    context = {
        'music': music
    }

    return render(request, "home.html", context)


@login_required(login_url="landingPage:signin")
def share(request, id_):
    music_form = Music_Form

    if (request.method == 'POST'):
        Music.objects.create(
            title=request.POST.get('title'),
            artist=request.POST.get('artist'),
            link=request.POST.get('link'),
            images=request.POST.get('images'),
            owner=User.objects.get(id=id_)
        )
        return HttpResponseRedirect('/music')

    context = {
        'music_form': music_form
    }

    return render(request, 'share.html', context)
