from django.db import models


class resep(models.Model):
    nama = models.CharField(max_length=120, null=True)
    bahan = models.CharField(max_length=700, null=True)
    alat = models.CharField(max_length=700, null=True)
    langkah = models.CharField(max_length=1500, null=True)

class Post(models.Model):
    title = models.CharField(max_length=50)
    bahan = models.CharField(max_length=700, null=True)
    alat = models.CharField(max_length=700, null=True)
    langkah = models.CharField(max_length=1500, null=True)

    def __str__(self):
        return self.title

# Create your models here.
