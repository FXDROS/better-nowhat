from django.shortcuts import render, HttpResponseRedirect
from django.http import JsonResponse
from .models import resep,Post
from .forms import buatresep
from django.contrib.auth.decorators import login_required

@login_required(login_url="landingPage:signin") 
def index(request):
    return render(request, "health.html")

@login_required(login_url="landingPage:signin") 
def inputform(request):
    kumpulanresep = resep.objects.all()
    response = {
        'reseps': kumpulanresep
    }
    return render(request, "isidong.html", response)
# Create your views here.

@login_required(login_url="landingPage:signin") 
def mintaresep(request):
    form = buatresep()
    if request.method == 'POST':
        form = buatresep(request.POST)

        resep.objects.create(
            nama=request.POST['nama'],
            bahan=request.POST['bahan'],
            alat=request.POST['alat'],
            langkah=request.POST['langkah'],

        )

        return HttpResponseRedirect('/health/isiform')

    response = {
        'form': form
    }
    return render(request, "request.html", response)

def create_post(request):
    posts = Post.objects.all()
    response_data = {}

    if request.POST.get('action') == 'post':
        title = request.POST.get('title')
        bahan= request.POST.get('bahan')
        alat= request.POST.get('alat')
        langkah=request.POST.get('langkah')

        response_data['title'] = title
        response_data['bahan'] = bahan
        response_data['alat'] = alat
        response_data['langkah'] = langkah
      
        Post.objects.create(
            title = title,
            bahan = bahan,
            alat = alat,
            langkah = langkah
            )
        return JsonResponse(response_data)

    return render(request, 'resep.html', {'posts':posts}) 