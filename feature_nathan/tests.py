from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve, reverse
from django.test import client
from django.apps import apps
from .models import resep,Post
from .views import index, inputform, mintaresep,create_post
from .forms import buatresep
from .apps import FeatureNathanConfig
import json


class TestApp(TestCase):
    def test_app_is_exist(self):
        self.assertEqual(FeatureNathanConfig.name, 'feature_nathan')
        self.assertEqual(apps.get_app_config(
            'feature_nathan').name, 'feature_nathan')


class TestIndex(TestCase):
    def setUp(self):
        self.client = Client()
        self.userData = {
            'username': 'demo',
            'email': 'apaaja@gmail.com',
            'password1': 'ssrifdiza',
            'password2': 'ssrifdiza',
        }
        self.mintaresep = reverse("feature_nathan:mintaresep")
        self.inputform = reverse("feature_nathan:inputform")
        self.user = User.objects.create_user(
            'demo', 'apaaja@gmail.com', 'ssrifdiza')
        self.login = {
            'username': 'demo',
            'password': 'ssrifdiza'
        }

        self.client.post('/signin/', data=self.login, follow=True)

        

    
    def test_url_bantuan_is_exist2(self):
        response = self.client.post(self.inputform)
        self.assertEqual(response.status_code, 200)
        
    def test_url_bantuan_is_exist(self):
        response = self.client.post('/health/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_bantuan_is_exist3(self):
        response = self.client.get(self.mintaresep)
        self.assertEqual(response.status_code, 200)

    def test_index_template(self):
        response = Client().get('/health/')
        # self.assertTemplateUsed(response, 'health.html')
    
    def test_json_function(self):
        data = {
            "title" : "perak",
            "bahan" : "2000-02-01",
            "alat" : "fun",
            "langkah" : "0812 aaya"
            }
        response = self.client.post('/health/resep/', json.dumps(data), content_type="application/json")
        self.assertEqual(response.status_code, 200)



class TestIsiForm(TestCase):
    

    def test_isiform_template(self):
        response = Client().get('/health/isiform/')
        # self.assertTemplateUsed(response, 'isidong.html')


class TestMintaResep(TestCase):
    def setUp(self):
        mintaresep = resep(nama='nasgor')
        self.assertEqual(resep.objects.all().count(), 0)

    def test_mintaresep_template(self):
        response = Client().post('/health/mintaresep/',
                                 {'nama': 'nasgor', 'bahan': 'apa ya?', 'alat': 'apa', 'langkah': 'gofud'})
        self.assertEqual(response.status_code, 302)

    def test_model_can_create_resep(self):
        newQuestion = resep.objects.create(
            nama="nasgor", bahan="apa ya?", alat="apa", langkah="gofud")
        counting_all_question = resep.objects.all().count()
        self.assertEqual(counting_all_question, 1)

    def test_model_can_create_Post(self):
        newQuestion = Post.objects.create(
            title="nasgor", bahan="apa ya?", alat="apa", langkah="gofud")
        counting_all_question = Post.objects.all().count()
        self.assertEqual(counting_all_question, 1)

    def test_resep_template(self):
        response = Client().post('/health/resep/',
                                 {'title': 'nasgor', 'bahan': 'apa ya?', 'alat': 'apa', 'langkah': 'gofud'})
        self.assertEqual(response.status_code, 200)

    def test_url_bantuan_is_exist4(self):
        response = Client().get('/health/resep/')
        self.assertEqual(response.status_code, 200)



# class TestEditResulutionTextView(TestCase):

#     def test_post(self):
#         expected_resolution = # put here what you are expecting
#         response = self.client.post('/health/resep/', **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
#         self.assertEqual(response.json(), {'title': expected_title})